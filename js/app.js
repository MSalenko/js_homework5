// 1. Метод об'єкту це функція, яка є властивістю об'єкта;
// 2. Значення властивості об'єкта може бути будь-якого типу;
// 3. Об'єкт має в собі ключі від різноманітних даних.

function createNewUser() {
  const name = prompt('What is your name?');
  const surname = prompt('What is your surname?');
  newUser = {
    firstName: name,
    lastName: surname,
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    set setFirstName(newName) {
      Object.defineProperty(newUser, 'firstName', { writable: true });
      this.firstName = newName;
      Object.defineProperty(newUser, 'firstName', { writable: false });
    },
    set setLastName(newName) {
      Object.defineProperty(newUser, 'lastName', { writable: true });
      this.lastName = newName;
      Object.defineProperty(newUser, 'lastName', { writable: false });
    },
  };
  return newUser;
}
createNewUser();
Object.defineProperty(newUser, 'firstName', { writable: false });
Object.defineProperty(newUser, 'lastName', { writable: false });
console.log(newUser.getLogin());
